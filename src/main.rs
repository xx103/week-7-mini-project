use std::env;

#[derive(Debug)]
struct StudentRecord {
    student_name: String,
    exam_grades: Vec<u8>,    // Assuming grades are out of 100
    homework_grades: Vec<u8>,
    project_grades: Vec<u8>,
}

struct GradeDB {
    records: Vec<StudentRecord>,
}

impl GradeDB {
    fn new() -> GradeDB {
        GradeDB { records: Vec::new() }
    }

    fn add_record(&mut self, student_name: &str, exam_grades: Vec<u8>, homework_grades: Vec<u8>, project_grades: Vec<u8>) {
        let record = StudentRecord {
            student_name: student_name.to_string(),
            exam_grades,
            homework_grades,
            project_grades,
        };
        self.records.push(record);
    }

    // Example query function: Find all records with an exam average above a certain threshold
    fn find_by_exam_average(&self, min_average: f32) -> Vec<&StudentRecord> {
        self.records.iter().filter(|record| {
            let sum: u32 = record.exam_grades.iter().map(|&grade| grade as u32).sum();
            let count = record.exam_grades.len() as u32;
            (sum as f32 / count as f32) >= min_average
        }).collect()
    }

    // Add more functions as needed for filtering, aggregation, etc.
}

fn main() {
    let args: Vec<String> = env::args().collect();
    
    // Example usage and setup for demonstration purposes
    if args.len() < 2 {
        eprintln!("Usage: cargo run -- <minimum exam average>");
        return;
    }

    let min_average = args[1].parse::<f32>().expect("Minimum exam average must be a valid number");

    let mut db = GradeDB::new();
    db.add_record("Alice", vec![90, 85, 92], vec![88, 90], vec![93]);
    db.add_record("Bob", vec![70, 75, 65], vec![80, 78], vec![76]);
    db.add_record("Charlie", vec![95, 98, 97], vec![100, 95], vec![99]);
    db.add_record("Angela", vec![94, 90, 91], vec![79, 98], vec![86]);
    db.add_record("Lisa", vec![75, 78, 77], vec![90, 73], vec![79]);
    db.add_record("John", vec![85, 92, 99], vec![86, 83], vec![90]);
    db.add_record("Cynthia", vec![96, 86, 93], vec![79, 98], vec![86]);
    db.add_record("Mark", vec![90, 75, 98], vec![90, 73], vec![79]);
    db.add_record("Sam", vec![100, 72, 89], vec![86, 83], vec![90]);


    let filtered_records = db.find_by_exam_average(min_average);
    println!("Students with an exam average above {}: ", min_average);
    for record in filtered_records {
        println!("{:?}", record);
    }
}
