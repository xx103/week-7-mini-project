
# Week 7 Mini-project: Grade Database System

This Rust application implements a simple grade database system that ingests student grade data, allows for querying based on certain criteria, and demonstrates how to structure the output. This system is a foundational example that can be extended to include more complex queries, aggregations, and visualizations.

## Requirements

The system is designed to fulfill the following requirements:

- **Ingest Data into Vector Database**: Load student records into an in-memory vector database.
- **Perform Queries and Aggregations**: Query the database to filter student records based on certain criteria, such as exam grade averages.
- **Visualize Output**: While the current implementation focuses on console output, it lays the groundwork for future integration with visualization libraries for richer data representation.


## Running the Application

1. Create a new project with `cargo lambda new project_name`.
2. Update `main.rs` and `Cargo.toml`.
3. Build the project with `cargo build`.
4. Run the application, providing a minimum exam average as an argument. For example:

```sh
cargo run -- 90
```

This command will execute the application, filtering and displaying records of students with an exam average above 90.

Output:
![Function overview](screenshot1.png)


## Sample Code

### Defining the Data Structure

This Rust code defines a `StudentRecord` struct with fields for storing a student's name and their grades for exams, homework, and projects. The `#[derive(Debug)]` attribute allows instances of this struct to be printed using the `{:?}` formatter, aiding in debugging and display.

```rust
#[derive(Debug)]
struct StudentRecord {
    student_name: String,
    exam_grades: Vec<u8>,    
    homework_grades: Vec<u8>,
    project_grades: Vec<u8>,
}
```

### Ingesting Data

The code below initializes a new `GradeDB` object and populates it with student records, each containing a name and lists of grades for exams, homework, and projects. It demonstrates adding multiple student records to the database for future querying and analysis.

```rust
let mut db = GradeDB::new();
db.add_record("Alice", vec![90, 85, 92], vec![88, 90], vec![93]);
db.add_record("Bob", vec![70, 75, 65], vec![80, 78], vec![76]);
db.add_record("Charlie", vec![95, 98, 97], vec![100, 95], vec![99]);
// Add more records...
```

### Querying the Database

The `find_by_exam_average` function iterates over student records, calculating the average exam grade for each. It filters records based on whether their average meets a specified minimum. The result is a vector of references to records that pass this filter, allowing identification of students with exam averages above the threshold.

```rust
fn find_by_exam_average(&self, min_average: f32) -> Vec<&StudentRecord> {
    self.records.iter().filter(|record| {
        let sum: u32 = record.exam_grades.iter().map(|&grade| grade as u32).sum();
        let count = record.exam_grades.len() as u32;
        (sum as f32 / count as f32) >= min_average
    }).collect()
}
```

### Running a Query

The following code snippet retrieves and prints student records with exam averages above a specified minimum. It calls `find_by_exam_average` to filter records, then iterates over the resulting vector, printing each qualifying student record alongside a message indicating the minimum average grade threshold.

```rust
let filtered_records = db.find_by_exam_average(min_average);
println!("Students with an exam average above {}: ", min_average);
for record in filtered_records {
    println!("{:?}", record);
}
```
